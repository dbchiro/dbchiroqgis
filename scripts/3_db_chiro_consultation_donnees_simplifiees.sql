-- données non détaillées (toutes données)
drop materialized view if exists mv_simplified_data
;

create materialized view mv_simplified_data
  as (
     select distinct
                     ss.id_sighting
                   , ses.date_start date
                   , EXTRACT(year from ses.date_start) an
                   , EXTRACT(month from ses.date_start) mois
                   , s2.id_place
                   , s2.name localite
                   , st_x(st_transform(s2.geom, 2154)) :: int x_l93
                   , st_y(st_transform(s2.geom, 2154)) :: int y_l93
                   , s2.altitude
                   , m2.name commune
                   , m2.code insee
                   , territory.code dpt
                   , case
                      when tp.category ilike 'tree'then 'Arbre'
                      when tp.category ilike 'building' then 'Bâtiment'
                      when tp.category ilike 'cliff' then 'Falaise'
                      when tp.category ilike 'nest' then 'Gîte artificiel'
                      when tp.category ilike 'bridge' then 'Pont'
                      when tp.category ilike 'cave' then 'Grotte'
                      when tp.category ilike 'other' then 'Autre'
                     else null end gite_simpl
                   , tp.descr gite_detail
                   , ds.codesp
                   , ds.common_name_fr nom_fr
                   , ds.sci_name nom_scien
                   , bool_and(ds.sp_true) sp_vraie, ss.period
                   , case
                      when c2.code = 'du' and (ss.total_count is null or ss.total_count=1) then sum(sc.count)
                      else ss.total_count end nombre
                   , c2.code contact
                   , ss.breed_colo colo_repro
                   , ses.comment commentaire_session
                   , ss.comment commentaire_obs
                   , string_agg(distinct (a.last_name || ' ' || a.first_name), ', ') observateurs
                   , mm.name etude
                   , stat.dhff
                   , stat.fr_regra_redlist lrra
                   , s2.geom
     from sights_sighting ss
            left join dicts_specie ds on ss.codesp_id = ds.id
            left join sights_session ses on ss.session_id = ses.id_session
            left join sights_session_other_observer ssoo on ssoo.session_id = ses.id_session
            left join accounts_profile a on (ses.main_observer_id = a.id or ssoo.profile_id = a.id)
            left join sights_place s2 on ses.place_id = s2.id_place
            left join dicts_typeplace tp on s2.type_id = tp.id
            left join dicts_contact c2 on ses.contact_id = c2.id
            left join geodata_territory territory on s2.territory_id = territory.id
            left join dicts_speciestatus stat on stat.specie_id = ss.codesp_id
            left join geodata_municipality m2 on s2.municipality_id = m2.id
            left join management_study mm on mm.id_study = ses.study_id
            left join sights_countdetail sc on ss.id_sighting = sc.sighting_id
     group by ss.id_sighting, ses.date_start, s2.id_place, s2.name, m2.code, territory.code, tp.category, tp.descr
            , ds.codesp, ds.common_name_fr, ds.sci_name, ss.period, ss.total_count, m2.name, c2.code, ss.breed_colo
            , ses.comment, ss.comment, mm.name, stat.dhff, stat.fr_regra_redlist, s2.geom)
;
