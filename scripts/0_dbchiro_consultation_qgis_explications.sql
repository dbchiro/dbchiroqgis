/*Le projet QGIS permet la consultation de :
1 synthèses communales (couche surfacique = communes) :
	- nombre de données
	- nombre d'espèces
	- nombre de localités
	- amplitude (1ère et dernière année)
	- par espèce :
		- nb de données par période (estivage, hivernage, été)
		- nb de données par type de contact
		- nb de gîtes
		- gîte repro (oui ou non)
		- nb de localités connues
		- amplitude
		- observateurs
2 synthèse par gîte (couche ponctuelle) :
	- nom du gîte
	- nb d'espèces
	- nb de données
	- amplitude
	- par espèce :
		- par période : nb ind max et nombre de données
		- amplitude
		- observateurs
3 données simplifées (toutes données)
4 données détailllées de capture

Pour ces infos, il y a en tout :
	- une couche commune avec les infos génériques (nb données...amplitude)
	- une couche de gîtes avec les infos génériques (nom gîte...amplitude)
	- une couche non géographique avec les détails par espèces par commune
	- une couche non géographque avec les détails par espèces et par gîte.
	- une couche géographique pour les données simplifiée
	- une couche géographique pour les données de capture
Les liens sont faits dans les propriétés du projets sur l'identifiant de la commune ou de la localité, dans les relations du projet qgis.

Les requêtes correspondantes sont :
1_dbchiro_consultation_synth_communes.sql
2_dbchiro_consultation_synth_gites.sql
3_db_chiro_consultation_donnees_simplifiees.sql
4_db_chiro_consultation_donnees_capture.sql

Les couches étant des vues matérialisées, il est nécessaire de paramétrer leur raffraichissement (ici toutes les heures) pour prendre en compte les nouvelles données.
Voici les commandes entrées dans le crontab de l'utilisateur root
#pour la synthèse communale :
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.synth_comm_detail;'"
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.synth_comm_list;'"
#pour la synthèse de gîtes :
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.synth_gite_list;'"
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.synth_gite_detail;'"
#pour la liste des données simplifiées
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.donnees_simplifiees;'"
#pour la liste des données de capture
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.donnees_capture;'"
*/


